function makeClone(obj) {
  let clone = {}; // Новый пустой объект для клона
  for (let prop in obj) { // Перебираем все свойства копируемого объекта
    if (obj.hasOwnProperty(prop)) { // Только собственные свойства, проверка чтоб не копировать ссылку вложеного объекта
      if ("object"===typeof obj[prop]) // Если свойство так же объект
      clone[prop] = makeClone(obj[prop]); // Делаем клон свойства - рекурсия
      else
      clone[prop] = obj[prop]; // Или же просто копируем значение
    }
  }
 return clone;
}
